#!/bin/sh

domain="$1"
shift 1

allow_read=""
allow_read="${allow_read}./"

allow_write=""
allow_write="${allow_write}./local/$domain/"

DENOFLAGS=""
DENOFLAGS="$DENOFLAGS --allow-net"
DENOFLAGS="$DENOFLAGS --allow-read=$allow_read"
DENOFLAGS="$DENOFLAGS --allow-write=$allow_write"
DENOFLAGS="$DENOFLAGS $@"

cd ..
mkdir -p "./local/$domain"
deno run $DENOFLAGS "./local/config.$domain.ts"
