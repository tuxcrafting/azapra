import { Azapra, WriterLogger, log } from '../mod.ts';

log.loggers.push(new WriterLogger);

const az = new Azapra({
    domain: /* your domain name here */,
});

await az.run();
