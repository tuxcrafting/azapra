import { Quad, QuadGraph, QuadObject, QuadPredicate, QuadSubject, Term, rdf } from './rdf.ts';

/**
 * Pattern to search for an RDF quad, each term is either a match or a
 * wildcard, except for the graph which must be provided.
 */
export type QuadPattern = [QuadSubject | null, QuadPredicate | null, QuadObject | null, QuadGraph];

function matchQuad1(pattern: Term | null, term: Term): boolean {
    return pattern === null || rdf.equals(pattern, term);
}

function matchQuad([s, p, o, g]: QuadPattern, quad: Quad): boolean {
    return (matchQuad1(s, quad.subject) &&
        matchQuad1(p, quad.predicate) &&
        matchQuad1(o, quad.object) &&
        matchQuad1(g, quad.graph));
}

/**
 * Generic dataset error.
 */
export class DatasetError extends Error { }

/**
 * Error when an ID doesn't exist in a dataset.
 */
export class DatasetIdError extends DatasetError { }

/**
 * Dataset interface. A dataset contains a set of RDF quads which can
 * be efficiently searched, each quad being identified by an unique
 * string ID. There may be quads with overlapping terms, but quads
 * cannot be duplicated.
 */
export interface Dataset {
    /**
     * Get a quad identified by its ID.
     */
    get(id: string): Promise<Quad>;

    /**
     * Insert a quad inside the dataset, return its ID.
     */
    insert(quad: Quad): Promise<string>;

    /**
     * Modify a quad identified by its ID.
     */
    modify(id: string, quad: Quad): Promise<void>;

    /**
     * Delete the quad identified by its ID.
     */
    delete(id: string): Promise<void>;

    /**
     * Search for quads matching a certain pattern, return their IDs.
     */
    match(pattern: QuadPattern): AsyncIterable<string>;

    /**
     * Begin a transaction. Transactions may be nested.
     */
    begin(): Promise<void>;

    /**
     * Commit all changes since the last begin.
     */
    commit(): Promise<void>;

    /**
     * Roll back all changes since the last begin.
     */
    rollback(): Promise<void>;
}

/**
 * Execute a function in a transaction within a Dataset, rolling back
 * on exceptions.
 */
export async function atomic<Type>(dataset: Dataset, callback: () => Promise<Type>): Promise<Type> {
    await dataset.begin();
    try {
        const r = await callback();
        await dataset.commit();
        return r;
    } catch (ex: any) {
        await dataset.rollback();
        throw ex;
    }
}

/**
 * Simple, unoptimized in-memory dataset implementation.
 */
export class TrivialDataset implements Dataset {
    private id: number = 0;
    private recs: Array<Record<string, Quad | undefined>> = [{}];

    private getSync(id: string): Quad {
        for (const rec of this.recs) {
            if (id in rec) {
                const quad = rec[id];
                if (quad === undefined)
                    break;
                return quad;
            }
        }
        throw new DatasetIdError(id);
    }

    async get(id: string): Promise<Quad> {
        return rdf.fromQuad(this.getSync(id));
    }

    async insert(quad: Quad): Promise<string> {
        const id = `${this.id++}`;
        this.recs[0][id] = rdf.fromQuad(quad);
        return id;
    }

    async modify(id: string, quad: Quad): Promise<void> {
        this.getSync(id);
        this.recs[0][id] = rdf.fromQuad(quad);
    }

    async delete(id: string): Promise<void> {
        this.getSync(id);
        if (this.recs.length === 1)
            delete this.recs[0][id];
        else
            this.recs[0][id] = undefined;
    }

    async* match(pattern: QuadPattern): AsyncIterable<string> {
        const visited = new Set;
        for (const rec of this.recs) {
            for (const id in rec) {
                if (visited.has(id))
                    continue;
                visited.add(id);
                const quad = rec[id];
                if (quad === undefined)
                    continue;
                if (matchQuad(pattern, quad))
                    yield id;
            }
        }
    }

    async begin(): Promise<void> {
        this.recs.unshift({});
    }

    async commit(): Promise<void> {
        const rec = this.recs.shift();
        for (const id in rec) {
            const quad = rec[id];
            if (quad === undefined && this.recs.length === 1)
                delete this.recs[0][id];
            else (quad !== undefined)
                this.recs[0][id] = quad;
        }
    }

    async rollback(): Promise<void> {
        this.recs.shift();
    }
}
