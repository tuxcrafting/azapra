import { Application, ListenOptions, Router, send } from 'https://deno.land/x/oak@v10.4.0/mod.ts';

import { Dataset, atomic } from '../dataset.ts';
import { Options as JsonLdOptions, RemoteDocument, toRDF } from '../jsonld.ts';
import { NamedNode, Quad, rdf } from '../rdf.ts';
import { log } from '../log.ts';
import { fromAsync } from '../util.ts';

export interface AzapraOptions {
    /** Listen options for the Oak backend. */
    listenOptions?: ListenOptions,

    /** Domain name on which the instance is hosted. */
    domain: string,

    /** Main dataset. */
    dataset: Dataset,
}

export const AzapraOptionsDef: Partial<AzapraOptions> = {
    listenOptions: {
        hostname: '127.0.0.1',
        port: 3000,
    },
};

const IRI = rdf.namedNode.bind(rdf);
function azURN(nss: string): NamedNode {
    return IRI(`urn:azapra:${nss}`);
}

/**
 * Main application class.
 */
export class Azapra {
    options: Required<AzapraOptions>;
    app: Application;

    private ds: Dataset;

    constructor(options: AzapraOptions) {
        this.options = { ...AzapraOptionsDef, ...options } as Required<AzapraOptions>;

        this.ds = this.options.dataset;

        this.app = new Application;

        this.app.use(async (ctx, next) => {
            await next();
            log.debug(`${ctx.response.status} ${ctx.request.method} ${ctx.request.url}`);
        });

        const router = new Router;

        router.get('/', async (ctx, next) => {
            await send(ctx, 'index.html', {
                root: `${Deno.cwd()}/web/`,
            });
        });

        router.get('/static/(.+)', async (ctx, next) => {
            try {
                await send(ctx, ctx.params['0'], {
                    root: `${Deno.cwd()}/web/static/`,
                });
            } catch {
                await next();
            }
        });

        this.app.use(router.routes());
        this.app.use(router.allowedMethods());
    }

    async jsonLdDocumentLoader(url: string): Promise<RemoteDocument> {
        const m = await fromAsync(this.ds.match([IRI(url), azURN('json'), null, azURN('docCache')]), 1);
        if (m.length == 1) {
            const doc = await this.ds.get(m[0]);
            return {
                contextUrl: undefined,
                documentUrl: url,
                document: JSON.parse(doc.object.value),
            };
        }

        try {
            const res = await fetch(url, {
                headers: {
                    'Accept': 'application/ld+json,application/json',
                },
            });
            const document = await res.json();

            await this.ds.insert(rdf.quad(
                IRI(url),
                azURN('json'),
                rdf.literal(JSON.stringify(document), IRI('http://www.w3.org/1999/02/22-rdf-syntax-ns#JSON')),
                azURN('docCache')));

            return {
                contextUrl: undefined,
                documentUrl: url,
                document: document,
            };
        } catch (ex) {
            await log.error(`failed fetching ${url}`);
            throw ex;
        }
    }

    async insertDocument(doc: Record<string, any>): Promise<void> {
        const quads = await toRDF(doc, {
            documentLoader: this.jsonLdDocumentLoader.bind(this),
        }) as Array<Quad>;

        atomic(this.ds, async () => {
            for (const quad of quads)
                this.ds.insert(quad);
        });
    }

    async run(): Promise<void> {
        const listenOptions = this.options.listenOptions;
        log.note(`Listening on ${listenOptions.hostname}:${listenOptions.port}`);
        await this.app.listen(listenOptions);
    }
}
