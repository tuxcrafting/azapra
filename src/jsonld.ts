import jsonld from 'https://esm.sh/jsonld@v5.2.0?bundle';

export type {
    Options,
    JsonLdDocument,
    NodeObject,
    GraphObject,
    ValueObject,
    ListObject,
    SetObject,
    LanguageMap,
    IndexMap,
    IdMap,
    TypeMap,
    IncludedBlock,
    ContextDefinition,
    ExpandedTermDefinition,
} from 'https://cdn.esm.sh/v66/@types/jsonld@1.5.6/index.d.ts';

export type {
    Frame,
    Url,
    RemoteDocument,
    JsonLdObj,
    JsonLdArray,
} from 'https://cdn.esm.sh/v66/@types/jsonld@1.5.6/jsonld-spec.d.ts';

export const {
    compact,
    expand,
    flatten,
    frame,
    normalize,
    canonize,
    fromRDF,
    toRDF,
    JsonLdProcessor,
} = jsonld;
