/**
 * @file
 * Implements the RDF data model, at
 * <https://rdf.js.org/data-model-spec/>.
 */

type TermType =
    'NamedNode' |
    'BlankNode' |
    'Literal' |
    'Variable' |
    'DefaultGraph' |
    'Quad';

export interface Term {
    readonly termType: TermType;
    value: string;
}

export interface NamedNode extends Term {
    readonly termType: 'NamedNode';
}

export interface BlankNode extends Term {
    readonly termType: 'BlankNode';
}

export interface Literal extends Term {
    readonly termType: 'Literal';
    language: string | undefined;
    datatype: NamedNode;
}

export interface Variable extends Term {
    readonly termType: 'Variable';
}

export interface DefaultGraph extends Term {
    readonly termType: 'DefaultGraph';
}

export type QuadSubject = NamedNode | BlankNode | Variable | Quad;
export type QuadPredicate = NamedNode | Variable;
export type QuadObject = NamedNode | Literal | BlankNode | Variable;
export type QuadGraph = DefaultGraph | NamedNode | BlankNode | Variable;

export interface Quad extends Term {
    readonly termType: 'Quad';
    subject: QuadSubject;
    predicate: QuadPredicate;
    object: QuadObject;
    graph: QuadGraph;
}

export class DataFactory {
    private blankNodeId: number = 0;

    private static defaultGraphInstance: DefaultGraph = {
        termType: 'DefaultGraph',
        value: '',
    };

    private static langStringDatatype: NamedNode = {
        termType: 'NamedNode',
        value: 'http://www.w3.org/1999/02/22-rdf-syntax-ns#langString',
    };

    private static stringDatatype: NamedNode = {
        termType: 'NamedNode',
        value: 'http://www.w3.org/2001/XMLSchema#string',
    };

    equals(a: Term, b: Term): boolean {
        if (a.termType !== b.termType)
            return false;

        if (a.termType === 'Literal') {
            const al = a as Literal;
            const bl = b as Literal;
            return al.value === bl.value &&
                al.language === bl.language &&
                this.equals(al.datatype, bl.datatype);
        } else if (a.termType === 'Quad') {
            const aq = a as Quad;
            const bq = b as Quad;
            return aq.value === bq.value &&
                this.equals(aq.subject, bq.subject) &&
                this.equals(aq.predicate, bq.predicate) &&
                this.equals(aq.object, bq.object) &&
                this.equals(aq.graph, bq.graph);
        } else {
            return a.value === b.value;
        }
    }

    namedNode(value: string): NamedNode {
        return {
            termType: 'NamedNode',
            value: value,
        };
    }

    blankNode(value?: string): BlankNode {
        if (value === undefined)
            value = `_:b${this.blankNodeId++}`;
        return {
            termType: 'BlankNode',
            value: value,
        };
    }

    literal(value: string, languageOrDatatype: string | NamedNode = DataFactory.stringDatatype): Literal {
        let language: string | undefined;
        let datatype: NamedNode;

        if (typeof languageOrDatatype === 'string') {
            language = languageOrDatatype;
            datatype = DataFactory.langStringDatatype;
        } else {
            language = undefined;
            datatype = languageOrDatatype;
        }

        return {
            termType: 'Literal',
            value: value,
            language: language,
            datatype: datatype,
        };
    }

    variable(value: string): Variable {
        return {
            termType: 'Variable',
            value: value,
        };
    }

    defaultGraph(): DefaultGraph {
        return DataFactory.defaultGraphInstance;
    }

    quad(subject: QuadSubject, predicate: QuadPredicate, object: QuadObject, graph: QuadGraph = DataFactory.defaultGraphInstance): Quad {
        return {
            termType: 'Quad',
            value: '',
            subject: subject,
            predicate: predicate,
            object: object,
            graph: graph,
        };
    }

    fromTerm(original: Term, recursive: boolean = false): Term {
        switch (original.termType) {
            case 'NamedNode':
                return this.namedNode(original.value);
            case 'BlankNode':
                return this.blankNode(original.value);
            case 'Literal':
                const originalLiteral = original as Literal;
                return this.literal(
                    original.value,
                    originalLiteral.language === '' ?
                        (recursive ?
                            this.fromTerm(originalLiteral.datatype, true) as NamedNode :
                            originalLiteral.datatype) :
                        originalLiteral.language);
            case 'Variable':
                return this.variable(original.value);
            case 'DefaultGraph':
                return this.defaultGraph();
            case 'Quad':
                const originalQuad = original as Quad;
                if (recursive)
                    return this.quad(
                        this.fromTerm(originalQuad.subject, true) as QuadSubject,
                        this.fromTerm(originalQuad.predicate, true) as QuadPredicate,
                        this.fromTerm(originalQuad.object, true) as QuadObject,
                        this.fromTerm(originalQuad.graph, true) as QuadGraph);
                else
                    return this.quad(
                        originalQuad.subject,
                        originalQuad.predicate,
                        originalQuad.object,
                        originalQuad.graph);
        }
    }

    fromQuad(original: Quad, recursive: boolean = false): Quad {
        return this.fromTerm(original, recursive) as Quad;
    }
}

export const rdf = new DataFactory;
