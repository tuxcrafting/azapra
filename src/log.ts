export enum LogLevel {
    debug = 'debug',
    note = 'note',
    warning = 'warning',
    error = 'error',
}

/**
 * A generic logger.
 */
export abstract class Logger {
    /**
     * Log a message at the given level at the given time.
     */
    abstract log(level: LogLevel, msg: string, time?: Date): Promise<void>;

    async debug(msg: string): Promise<void> { return this.log(LogLevel.debug, msg); }
    async note(msg: string): Promise<void> { return this.log(LogLevel.note, msg); }
    async warning(msg: string): Promise<void> { return this.log(LogLevel.warning, msg); }
    async error(msg: string): Promise<void> { return this.log(LogLevel.error, msg); }
}

type Formatter = (level: LogLevel, msg: string, time: Date) => string;

function defaultFormatter(level: LogLevel, msg: string, time: Date) {
    return `[${level.padEnd(7)} ${time.toISOString()}] ${msg}`;
}

/**
 * Logger writing to a file.
 */
export class WriterLogger extends Logger {
    private writer: Deno.Writer;
    private formatter: Formatter;
    private encoder: TextEncoder = new TextEncoder;

    /**
     * @param formatter Function producing a string given the input
     * parameters (no trailing newline).
     */
    constructor(writer: Deno.Writer = Deno.stdout, formatter: Formatter = defaultFormatter) {
        super();
        this.writer = writer;
        this.formatter = formatter;
    }

    async log(level: LogLevel, msg: string, time: Date = new Date): Promise<void> {
        const str = this.formatter(level, msg, time);
        await this.writer.write(this.encoder.encode(`${str}\n`));
    }
}

/**
 * Logger writing to multiple other loggers.
 */
export class MultiLogger extends Logger {
    minLevel: LogLevel = LogLevel.debug;
    loggers: Array<Logger> = [];

    async log(level: LogLevel, msg: string, time: Date = new Date): Promise<void> {
        await Promise.allSettled(this.loggers.map((x) => x.log(level, msg, time)));
    }
}

export const log = new MultiLogger;
