import { encode } from 'https://deno.land/std@0.127.0/encoding/base64.ts';
import { crypto } from 'https://deno.land/std@0.127.0/crypto/mod.ts';

export const idLength = 24;

export const emptyId = '0'.repeat(idLength);

/**
 * Generate a random identifier. The identifier is made of 24 Base64
 * digits.
 */
export function genId(): string {
    const buf = new Uint8Array(18);
    crypto.getRandomValues(buf);
    return encode(buf);
}

/**
 * Similar to Array.from, but on AsyncIterables.
 * @param max Maximum length of the returned array, defaults to -1 for
 * no maximum.
 */
export async function fromAsync<Type>(it: AsyncIterable<Type>, max: number = -1): Promise<Array<Type>> {
    const m = [];
    let i = 0;
    for await (const x of it) {
        if (i++ == max)
            break;
        m.push(x);
    }
    return m;
}
